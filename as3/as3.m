function [ output ] = as3(clusters)
% Chad Maughan CS6665
%   K-means clustering assignment 3

warning off;

data70 = xlsread('~/Desktop/cs6665/as3/h3-70.xls');
data70mb = xlsread('~/Desktop/cs6665/as3/h3-70-mb.xls'); % malignant/benign data (being punished for being a mactard)

while clusters > 1

    disp(clusters);
    
    data30 = xlsread('~/Desktop/cs6665/as3/h3-30.xls');
    data30mb = xlsread('~/Desktop/cs6665/as3/h3-30-mb.xls'); % malignant/benign data (being punished for being a mactard)

    [ids70 ctrs70 sumd70, d70] = kmeans(data70, clusters, 'distance', 'sqEuclidean');
    
    % calculates how simliar a point is to other points in its own cluster compared to other clusters 
    s = silhouette(data70, ids70, 'sqEuclidean');
    avg = mean(s);
    
    % disp(avg);
    
    correct = 0.0;
    
    % store the cluster row values
    values = zeros(3);
    
    % loop through the test data rows
    for i = 1:max(171)

        % calculate the euclidian distance
        D = dist( [ data30(i,:); ctrs70 ] );
        [distance pid] = min( D(1,2:end) );

        values(pid,1) = pid;
        
        result = data30mb(i,1);
        if(result == 1)
            values(pid,3) = (values(pid,3) + 1);
        else
            values(pid,2) = (values(pid,2) + 1);
        end

    end

    % disp(values);
    
    % get the malignant/benign value to test (I converted M=1, B=0)
    for j = 1:size(values)
        if(values(j,1) > 0)
            right = max(values(j,2), values(j,3)) / (values(j,2) + values(j,3));
            disp(right);
        end
    end
    
    % decrement (by half)
    clusters = round(clusters / 2);

end

output = 0;

end


