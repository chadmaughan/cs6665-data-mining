function [ output ] = as4( input )
    
    % RMSE
    output=0;

    % create array for difference of xls(G) and value
    data = zeros(1,10)

    % get the values from the binary chromosone (input)
    %   (hardcoded because we know the bit length of each gene)
    P = as4Bin2Dec(input(1:6));
    Q = as4Bin2Dec(input(7:13));
    R = as4Bin2Dec(input(14:20));
    S = as4Bin2Dec(input(21:27));
    T = as4Bin2Dec(input(28:33));
    U = as4Bin2Dec(input(34:39));
    V = as4Bin2Dec(input(40:45));
    W = as4Bin2Dec(input(46:51));

    % for getting the value of the optimum (for paper)
    disp(P);
    disp(Q);
    disp(R);
    disp(S);
    disp(T);
    disp(U);
    disp(V);
    disp(W);

    % read in the H4 data
    x = xlsread('~/Desktop/cs6665/as4/H4Data.xls')
    
    % calculate the number of rows
    rows = length(x(:,1));

    for row = 1:rows,
        
        % get values of A...F
        A = x(row,1);
        B = x(row,2);
        C = x(row,3);
        D = x(row,4);
        E = x(row,5);
        F = x(row,6);

        G = x(row,7);

        value = P * sin(A) + Q * log(A) + R * abs(B/C) + S * D + T * exp(U*E) + U * V * cos((E)^2) + W * log(F);

        % store the difference for later calculation of RMSE
        data(row) = abs(G - value);
        
        disp(G);
        disp(value);
        disp(abs(G-value));
    end
    
    % calculate the RMSE (cheat, 10 references the number of known rows in the xls spreadsheet)
    output = sqrt(sum(data.^2)/10)
end

function [output] = as4Bin2Dec(portion)

    output = 0;

    % convert from vector to string
    s = strrep(int2str(portion),' ','');
    
    % from lecture notes
    j = 1; 
    L = length(s); 
    while(j <= L)
        output = output * 2 + bin2dec(s(j));
        j = j + 1;
    end
end